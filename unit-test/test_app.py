import unittest
import datetime
import json

def call(client, path, params):
    url = path + '?' + urlencode(params)
    response = client.get(url)
    return json.loads(response.data.decode('utf-8'))
def test_time(client):
    t = datetime.date.today()
    result = call(client, '/time=1')
    assert result['t'] == datetime.date.today()
def test_whoami(client):
    result = call(client, '/whoami=1')
    assert result['ip_address'] == 'ip_adress'
